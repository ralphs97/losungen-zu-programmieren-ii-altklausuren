﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS13._2
{
    class Generisches
    {
        public static T[] SwapArray<T>(T[] array, int a, int b)
        {
            T merke = array[a];
            array[a] = array[b];
            array[b] = merke;
            return array;
        }

        public static void A2()
        {
            string[] s = { "Anton", "Berthold", "Caroline", "Dieter", "Emil" };

            string[] ret = SwapArray(s, 0, s.Length - 1);

            foreach (string str in ret)
                Console.WriteLine(str);

            /* 2.2 */
            Factory<Zeit> f = new Factory<Zeit>();
        }

        public interface IFactoryable
        {
            void Show(string argument);
        }

        class Factory<T> where T : IFactoryable, new()
        {
            public T Create(string arg)
            {
                if (arg == null)
                    throw new ArgumentNullException();
                T myFactory = new T();
                myFactory.Show(arg);
                return myFactory;
            }
        }

        class Zeit : IFactoryable
        {
            int std, min;
            public Zeit() { }
            public Zeit(int s, int m)
            {
                std = s;
                min = m;
            }
            public void Show(string argument)
            {
                Console.WriteLine(argument);
            }
        }

        /* 2.3 */

        class BaseNodeMultiple<T, U>
        {

        }

        class Node1<T> : BaseNodeMultiple<T, int> { }
        class Node2<T, U> : BaseNodeMultiple<T, U> { }
        /* Geht nicht: class Node3<T> : BaseNodeMultiple<T, U> { } */

    }
}

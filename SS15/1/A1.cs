﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS15._1
{
    class A1
    {
        public static void Eins()
        {
            Stack<int> longStack = new Stack<int>();
            Stack<int> threeStack = new Stack<int>();
            Stack<int> twoStack = new Stack<int>();
            Stack<int> oneStack = new Stack<int>();
            Stack<int> emptyStack = new Stack<int>();

            // Fill the stacks
            for (int i = 0; i < 10; i++)
            {
                longStack.Push(i);
            }
            for (int i = 1; i < 4; i++)
            {
                threeStack.Push(i);
            }
            for (int i = 20; i < 22; i++)
            {
                twoStack.Push(i);
            }
            oneStack.Push(999);

            // Add them to a list for convenience
            List<Stack<int>> stacks = new List<Stack<int>>
            {
                longStack,
                threeStack,
                twoStack,
                oneStack,
                emptyStack
            };

            // Test the Pop() and Enumerator method 
            /*
            foreach(Stack<int> stack in stacks)
            {
                Console.WriteLine($"pre pop: {stack.ToString()}");
                try
                {
                    stack.Pop();
                }
                catch(ArgumentNullException)
                {
                    Console.WriteLine("Caught ArgumentNullException");
                }
                Console.WriteLine($"post pop: {stack.ToString()}\n");
            }
            */
            //Test the Revert() method
            foreach (Stack<int> stack in stacks)
            {
                Console.WriteLine($"pre revert: {stack.ToString()}");
                stack.Revert();
                Console.WriteLine($"post revert: {stack.ToString()}\n");
            }
        }
    }

    public class Stack<T>
    {
        private class Node
        {
            public T Payload { get; set; }
            public Node Next { get; set; }
            public Node(T payload)
            {
                Payload = payload;
            }
        }
        Node headOfStack;
        
        /* 1 */
        public void Push(T item)
        {
            Node insertion = new Node(item);
            insertion.Next = headOfStack;
            headOfStack = insertion;
        }

        /* 2 */
        public T Pop()
        {
            if(headOfStack == null) { throw new ArgumentNullException(); }
            T ret = headOfStack.Payload;
            headOfStack = headOfStack.Next;
            return ret;
        }

        /* 3 */
        public IEnumerator GetEnumerator()
        {
            Node pointer = headOfStack;
            while(pointer != null)
            {
                yield return pointer.Payload;
                pointer = pointer.Next;
            }
        }

        /* 4 */
        public void Revert()
        {
            if (headOfStack == null || headOfStack.Next == null)
                return;

            Node schlepp = headOfStack;
            Node pointer = schlepp.Next;
            schlepp.Next = null;

            while(pointer != null)
            {
                Node merke = pointer.Next;
                pointer.Next = schlepp;
                schlepp = pointer;
                pointer = merke;
            }
            headOfStack = schlepp;
        }

        public override string ToString()
        {
            string items = "";
            foreach(T item in this)
            {
                items += $"{item.ToString()}, ";
            }
            return (items.Length > 2) ? $"[{items.Remove(items.Length - 2)}]" : "[]";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS15._2
{
    class Program
    {
        public static void A2()
        {
            int[] b = { 1, 6, 9 };

            if(Teilmenge(new int[] { 8, 6, 1, 7, 4, 9 }, b))
            {
                Console.WriteLine("A enthält B");
            }
            else
            {
                Console.WriteLine("A enthält B nicht.");
            }
        }

        public static bool Teilmenge<T>(T[] a, T[] b)
        {
            bool[] contains = new bool[b.Length];
            int i = 0;
            foreach(T elementB in b)
            {
                foreach(T elementA in a)
                {
                    if (elementB.Equals(elementA))
                        contains[i] = true;
                }
                i++;
            }
            foreach(bool val in contains)
            {
                if (val == false)
                    return false;
            }
            return true;
        }
    }
}

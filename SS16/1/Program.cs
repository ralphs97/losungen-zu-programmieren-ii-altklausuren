﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SommerSemester16._1
{
    class Program
    {
        /*
        public static void Main(string[] args)
        {
            Aufgabe1a();
            Aufgabe1b();
        }
        */

        private static void Aufgabe1b()
        {
            Console.WriteLine("\n\n1 b):");
            string loesung1b = @"
                public static Punkt2D operator ++(Punkt2D p)
                {
                    return new Punkt2D(p.x+1,p.y+1);
                }";
            Console.WriteLine(loesung1b);
        }

        private static void Aufgabe1a()
        {
            Console.WriteLine("1 a):");
            Punkt3D p1 = new Punkt3D(1, 1, 6);
            p1 += 5;
            Punkt2D p2 = p1;
            p2 = 5 + p2;
            if (p1)
            {
                Console.Write("E ");
            }
            p2 = 3;
            if (!p2)
            {
                Console.Write("F ");
            }
        }
    }
}


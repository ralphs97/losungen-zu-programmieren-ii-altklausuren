﻿using System;

namespace SommerSemester16._1
{
    class Punkt2D
    {
        protected int x, y;
        public Punkt2D(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static Punkt2D operator +(Punkt2D p, int summand)
        {
            Console.Write("A ");
            return new Punkt2D(p.x + summand, p.y + summand);
        }
        public static Punkt2D operator +(Punkt2D p1, Punkt2D p2)
        {
            Console.Write("B ");
            return new Punkt2D(p1.x + p2.x, p1.y + p2.y);
        }
        public static implicit operator Punkt2D(int x)
        {
            Console.Write("C ");
            return new Punkt2D(x, 0);
        }
        public static bool operator true(Punkt2D p)
        {
            if (p.x > 0 && p.y > 0) return true;
            else return false;
        }
        public static bool operator false(Punkt2D p)
        {
            return !true;
        }
        public static bool operator !(Punkt2D p)
        {
            return false;
        }

        public static Punkt2D operator ++(Punkt2D p)
        {
            return new Punkt2D(p.x+1,p.y+1);
        }
    }
}
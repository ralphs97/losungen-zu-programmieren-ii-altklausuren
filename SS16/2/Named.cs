﻿namespace SommerSemester16._2
{
    interface Named
    {
        string Name { get; }
    }
}
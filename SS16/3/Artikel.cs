namespace SommerSemester16._3
{
    /*>*/ public abstract /*<*/ class Artikel : IArtikel
    {
        public abstract double betrag /*>*/ { get; set; } /*<*/
        public string bezeichnung /*>*/ { get; } /*<*/

        public Artikel(string Bezeichnung)
        {
            /*>*/ bezeichnung = Bezeichnung; /*<*/
        }
    }
}
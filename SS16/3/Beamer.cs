using System;

namespace SommerSemester16._3
{
    /*>*/ public /*<*/ class Beamer : Elektrogeraete
    {
        protected string aufloesung;

        public Beamer(String Bez, double Betrag, double Strom, string Aufloesung)
            /*>*/ : base(Bez, Betrag, Strom) /*<*/
        {
            /*>*/ aufloesung = Aufloesung; /*<*/
        }
    }
}
namespace SommerSemester16._3
{
    /*>*/ public /*<*/ class Elektrogeraete : Artikel
    {
        protected double stromverbrauch;

        public Elektrogeraete(string Bezeichnung, double Betrag, double Stromverbrauch)
            /*>*/ : base(Bezeichnung) /*<*/
        {
            /*>*/ stromverbrauch = Stromverbrauch; /*<*/
            /*>*/ betrag = Betrag; /*<*/
        }

        public /*>*/ override double betrag /*>*/ { get; set; } /*<*/
    }
}
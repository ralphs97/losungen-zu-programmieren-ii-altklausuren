namespace SommerSemester16._3
{
    public interface IArtikel
    {
        string bezeichnung { get; }
        double betrag { get; set; }
    }
}
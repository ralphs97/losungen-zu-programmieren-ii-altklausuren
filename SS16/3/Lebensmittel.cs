namespace SommerSemester16._3
{
    /*>*/ public /*<*/ class Lebensmittel : Artikel
    {
        public Lebensmittel(string Bezeichnung, double Betrag, string Haltbarkeitsdatum)
            /*>*/ : base(Bezeichnung) /*<*/
        {
            betrag = Betrag;
        }

        protected string haltbarkeitsdatum;

        public /*>*/ override /*<*/ double betrag /*>*/ { get; set; } /*<*/

        public /*>*/ override /*<*/ string ToString()
        {
            return /*>*/ bezeichnung; /*<*/
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SommerSemester16._4
{
    class Program
    {
        class MyClass
        {
            /*
            static void Main()
            {
                MyClass myClass = new MyClass();
                myClass.MyClassMethod();
                Console.Write("END");
            }
            */

            public MyClass()
            {
                try
                {
                    MyMethod();
                    Console.Write("A");
                }
                catch (DivideByZeroException) { Console.Write("B"); }
                catch (NullReferenceException) { Console.Write("C"); }
                catch (IndexOutOfRangeException) { Console.Write("D"); }
                catch (Exception) { Console.Write("E"); }
                Console.Write("F");
            }
            public void MyClassMethod() { Console.Write(texts[0]); }
            string[] texts;
        }

        static void MyMethod()
        {
            try
            {
                int[] values = new int[] { 5, 6, 7, 0 };
                MyClass myClass = null;
                Console.Write(values[3] / values[0]);
                Console.Write(values[0]);
                myClass.MyClassMethod();
            }
            catch (DivideByZeroException e) { Console.Write("1"); throw e; }
            catch (IndexOutOfRangeException e) { Console.Write("2"); throw e; }
            finally { Console.Write("3"); }
            Console.Write("4");
        }
    }
}

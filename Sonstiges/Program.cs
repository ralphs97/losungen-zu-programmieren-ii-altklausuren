﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sonstiges
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        public class DoublyLinkedList
        {
            public class Node
            {
                public Node Previous { get; set; }
                public Node Next { get; set; }
                public object Payload { get; set; }
            }

            Node head;

            public void Add(object data)
            {
                var addition = new Node()
                {
                    Previous = null,
                    Next = head,
                    Payload = data
                };

                head.Previous = addition;
                head = addition;
            }
        }
    }
}

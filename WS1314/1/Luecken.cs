﻿using System;

namespace WS1314._1
{
    public class Luecken
    {
        public static void A1()
        {
            Fish fi = new Shark("alpha", "i am bad");
            Shark fi2 = new Shark("bravo", "i am good");
            Shark fi3 = new Sharktopus("charly", "I am not a goldfish");
            SeaObject fi4 = new Sharktopus("delta", "friendly");
            Console.WriteLine(fi.Touch());
            Console.WriteLine(fi.Name);
            Console.WriteLine(fi2.Touch ());
            Console.WriteLine(fi2.Name);
            Console.WriteLine(fi3.Touch ());
            Console.WriteLine(fi3.Name);
            Console.WriteLine(fi4.Touch ());
            Console.WriteLine((fi4 as Fish).Name);
        }
        
        public abstract class SeaObject
        {
            public String Description { get; private set; }
            public abstract String Touch();
            public SeaObject (String desc)
            {
                Description = desc;
            }
        }
        
        public class Fish : SeaObject
        {
            public virtual String Name {get; private set;}
            public override String Touch(){return "Blup";}
            public Fish (String name, String desc) : base(desc)
            { Name = name; }
        }
        
        public class Shark : Fish
        {
            public override String Touch(){return "nom nom nom";}
            public Shark (String name, String desc) : base(name, desc) { }
            public override String Name
            { get {return "Big Shark " + base.Name ; } }
        }
        
        public class Sharktopus : Shark
        {
            public String Touch()
            {
                return "best friend of human under water";
            }
            public override String Name
            {
                get {return "Best Friend " + base.Name ; }
            }
            public Sharktopus(String name, String desc) : base(name, desc)
            { }
        }
    }
}
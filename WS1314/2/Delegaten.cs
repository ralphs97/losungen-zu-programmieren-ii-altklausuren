﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace WS1314._2
{
    public class Delegaten
    {
        /*
        Teil 1)
        public delegate float FloatToFloat(float f);

        public delegate string FloatToString(float f);
        */
        
        
        /* Teil 2) */
        public delegate double Calculate(double f);

        public delegate string Output(double f);

   
        public string DoStuff(double d, Calculate c, Output o) => o(c(d));
        
        
        /* Teil 3) */
        public delegate bool Filter(string s);
        
        public static void Teil3()
        {
            
            List<String> words = new List<string>();
            words.Add("Chuck Norris");
            words.Add("Bud Spencer");
            words.Add("Trevor Philips");
            words.Add("Michael Myers");
            words.Add("Batman");

            Filter a = delegate(string name) { return name.Contains(" "); };
            Filter b = name => name.Contains(" ");
        }
        
        /* Teil 4) */
        public List<String> EineMethode(List<String> list, Filter f)
        {
            var r = new List<String>();
            foreach (String s in list)
            {
                if(f(s)) r.Add(s);
            }
            return r;
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1516._2
{
    class Program
    {
        public static void A2()
        {
            Stack<String> list = new Stack<String>();
            list.Push("A");
            list.Push("B");
            list.Push("C");

            list.Print();

            Console.WriteLine($"{list[1]} - should be B");
            Console.WriteLine($"{list[0]} - should be C");
            try
            {
                Console.WriteLine($"{list[3]} - shouldn't be Possible");
            }
            catch(ArgumentOutOfRangeException e)
            {
                Console.WriteLine($"Caught exception: {e.Message}");
            }
            

        }
    }
}

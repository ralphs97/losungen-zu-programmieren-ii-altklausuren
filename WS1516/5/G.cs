﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1516._5
{
    /* a */
    /*
    class G
    {
        public X Method(Y y)
        {
            y.Something();
            return new X();
        }
    }
    */
    class G<X, Y> where Y : IHasSomething
                    where X : new()
    {
        public X Method(Y y)
        {
            y.Something();
            return new X();
        }
    }

    public interface IHasSomething
    {
        void Something();
    }

        /* b */
    public interface IMyList<T> where T : IComparable, IEnumerable<T>
    {
        void Add(T element);
        void Add(T[] array);
        T Take();
        void Delete(T element);
    }

    /* c */
    class C<T> where T : IComparable
    {
        public static T ReturnGreater(T a, T b)
        {
            if (a.CompareTo(b) < b.CompareTo(a))
                return a;
            else
                return b;
        }
    }
    
}

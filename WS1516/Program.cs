﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1516
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------------ A1 ------------");
            _1.Program.A1();
            Console.WriteLine("------------ A2 ------------");
            _2.Program.A2();
            Console.WriteLine("------------ A3 ------------");
            _3.Program.A3();
            Console.WriteLine("------------ A4 ------------");
            _4.Delegates.A4();
        }
    }
}
